for x in 1..100 do
  case
    when (x%3 == 0) && (x%5 == 0) then puts "BitMaker"
    when x%3 == 0 then puts "Bit"
    when x%5 == 0 then puts "Maker"
    else puts x
  end
end
