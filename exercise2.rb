# Tip
cost_of_meal = 55
puts "Was the service good? Yes or No?"
answer = gets.chomp.downcase

if answer == "yes"
  puts "All of the tips! Final bill is: $#{cost_of_meal * 1.5}"
else
  puts "You're a hard cookie to please. Final bill is $#{cost_of_meal * 1 }"
end

# String interpolation
puts "And the number of thy counting shall be " + 3.to_s

# MOAR string interpolation
number = 45628 * 7839
puts "I think this might work, if I practise it #{number} times!"

# Should evaluate to true?
puts (10<20 && 30<20) || !(10==11)
